import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespuestaMDB, PeliculaDetalle, RespuestaCredits, Genre } from '../interfaces/interfaces';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

const URL = environment.url;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

    private popularesPage = 0;
    generos: Genre[] = [];

    constructor(private http: HttpClient){ 
    }

    private ejecutarQuery<T>(query: string){
        query = URL + query;
        query += `&api_key=${ apiKey }&language=es&include_image_language=es`;

        return this.http.get<T>(query);
    }

    getPopulares(){
        this.popularesPage++;

        return this.ejecutarQuery<RespuestaMDB>(`/discover/movie?sort_by=popularity.desc&page=${ this.popularesPage }`);
    }

    getFeature(){
        const startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
        const endOfMonth = moment().endOf('month').format('YYYY-MM-DD');

        return this.ejecutarQuery<RespuestaMDB>(`/discover/movie?primary_release_date.gte=${ startOfMonth }&primary_release_date.lte=${ endOfMonth }`);
    }

    getPeliculaDetalle(id: string){
        return this.ejecutarQuery<PeliculaDetalle>(`/movie/${ id }?a=1`);
    }

    getActoresPelicula(id: string){
        return this.ejecutarQuery<RespuestaCredits>(`/movie/${ id }/credits?a=1`);
    }

    getGeneros(): Promise<Genre[]>{
        return new Promise(resolve => {
            this.ejecutarQuery(`/genre/movie/list?a=1`).subscribe( resp => {
                this.generos = resp['genres'];
                resolve(this.generos);
            });
        });
    }

    buscarPelicula(texto: string){
        return this.ejecutarQuery(`/search/movie?query=${ texto }`);
    }
}
