import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { DataLocalService } from 'src/app/services/data-local.service';
import { PeliculaDetalle, Cast } from 'src/app/interfaces/interfaces';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {
    @Input() id;
    pelicula: PeliculaDetalle;
    actores: Cast[] = [];
    oculto = 150;
    existe: boolean;

    slideOptsActores = {
        slidesPerView: 3.3,
        freeMode: true,
        spaceBetween: -5
    }

    constructor(private _moviesService: MoviesService,
                private modalController: ModalController,
                private dataLocal: DataLocalService) { }

    async ngOnInit(){
        this.existe = await this.dataLocal.existePelicula(this.id);

        this._moviesService.getPeliculaDetalle(this.id).subscribe( resp => {
            this.pelicula = resp;
        });

        this._moviesService.getActoresPelicula(this.id).subscribe( resp => {
            this.actores = resp.cast;
        });
    }

    regresar(){
        this.modalController.dismiss();
    }

    favorito(){
        this.existe = this.dataLocal.guardarPelicula(this.pelicula);
    }

}
